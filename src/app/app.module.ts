import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// ROUTES

import { routes } from './app.routes';

import { AppComponent } from './app.component';
import { DataComponent } from './components/data/data.component';
import { TaskService } from './services/task.service';
import { HttpClientModule } from '@angular/common/http';
import { LandingComponent } from './components/landing/landing.component';
import { LoginComponent } from './components/login/login.component';
import { SignInComponent } from './components/sign-in/sign-in.component';

@NgModule({
  declarations: [AppComponent, DataComponent, LandingComponent, LoginComponent, SignInComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {}
