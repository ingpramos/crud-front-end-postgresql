import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users: any = [];
  newLogin: any = {};
  usernames: any = [];
  passwords: any = [];

  constructor(private _taskService: TaskService, private router: Router) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this._taskService.getUsers().subscribe((users: any) => {
      this.users = users.data;

      console.log(this.users);

      for (let i = 0; i < this.users.length; i++) {
        const username = this.users[i].username;
        const password = this.users[i].password;
        this.usernames.push(username);
        this.passwords.push(password);
      }
    });
  }

  loginUser(login: NgForm) {
    this.newLogin = login.value;

    for (let i = 0; i < this.users.length; i++) {
      if (
        this.newLogin.username == this.usernames[i] &&
        this.newLogin.password == this.passwords[i]
      ) {
        this.router.navigate(['data']);
        break;
      } else {
        this.router.navigate(['login']);
      }
    }
  }
}
