import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  users: any = [];
  user: any = {};
  constructor(private _taskService: TaskService) {}

  ngOnInit() {}

  addUser(user: NgForm) {
    const newUser = user.value;

    this._taskService.addUser(newUser).subscribe(user => {
      this.users.push(user);
    });
  }
}
