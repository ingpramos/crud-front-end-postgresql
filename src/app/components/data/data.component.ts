import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: []
})
export class DataComponent implements OnInit {
  tasks: any = [];
  task: any = {};
  info: any;

  constructor(private _taskService: TaskService) {
    // this._taskService.getTasks().subscribe(tasks => {
    //   this.tasks = tasks;
    //   console.log(tasks);
    // });
  }

  ngOnInit() {
    this.getTasks();
  }

  getTasks() {
    this._taskService.getTasks().subscribe((tasks: any) => {
      this.tasks = tasks.data;
      // console.log(this.tasks);
    });
  }

  addTask(data: NgForm) {
    // console.log('NgForm', formulario);
    // console.log('valor', data.value);

    const newTask = data.value;

    this._taskService.addTask(newTask).subscribe(task => {
      this.tasks.push(task);
      this.task = '';
      return this.getTasks();
    });
  }

  deleteTask(id: any) {
    const tasks = this.tasks;
    this._taskService.deleteTask(id).subscribe(data => {
      data;
      console.log(data);
      return this.getTasks();

      // for (let i = 0; i < tasks.length; i++) {
      //   if (tasks[i].id == id) {
      //     tasks.splice(i, 1);
      //   }
      // }
    });
  }

  editTask(id, form) {
    const newTask = {
      id: id,
      task: form.value
    };
    console.log(form.value);
    this._taskService.updateTask(newTask).subscribe(res => {
      console.log(res);
      return this.getTasks();
    });
  }
}
