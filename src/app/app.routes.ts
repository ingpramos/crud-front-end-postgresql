import { Routes, RouterModule } from '@angular/router';
// import { NgModule } from '@angular/core';

import { LandingComponent } from './components/landing/landing.component';
import { DataComponent } from './components/data/data.component';
import { LoginComponent } from './components/login/login.component';
import { SignInComponent } from './components/sign-in/sign-in.component';

export const routes: Routes = [
  { path: 'home', component: LandingComponent },
  { path: 'data', component: DataComponent },
  { path: 'login', component: LoginComponent },
  { path: 'sign', component: SignInComponent },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

// @NgModule({
//   imports: [RouterModule.forChild(routes)],
//   exports: [RouterModule]
// })
// export class FeatureRoutingModule {}
